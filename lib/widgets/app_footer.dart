import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MainAppFooter extends StatelessWidget {
  const MainAppFooter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Text("Copyright \u00a9 ${DateTime.now().year}",
            style: GoogleFonts.raleway(fontWeight: FontWeight.bold)),
      ),
      height: 40,
      color: Colors.blue[300],
    );
  }
}

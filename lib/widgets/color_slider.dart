import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/color_slider_controller.dart';
import '../enums/enums.dart';

class ColorSlider extends StatelessWidget {
  Color color;
  final ColorChannels channel;

  ColorSlider({Key? key, required this.color, required this.channel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        global: false,
        init: ColorSliderController(color),
        builder: (ColorSliderController ctrl) {
          return Slider(
              min: 0.0,
              max: ctrl.setMaxSlider(channel),
              divisions: ctrl.setDivisions(channel),
              label: ctrl.getLabel(channel),
              activeColor: Colors.green,
              inactiveColor: Colors.orange,
              value: ctrl.getSliderValue(channel),
              onChanged: (double value) {
                ctrl.updateSliderValue(value);
                ctrl.getFunc(channel)(value);
                ctrl.update();
              });
        });
  }
}

import 'package:color_back/controllers/color_slider_controller.dart';
import 'package:color_back/widgets/color_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../enums/enums.dart';


class ColorSettings extends StatelessWidget {
  final Color color;

  const ColorSettings({Key? key, required this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder(
      init: ColorSliderController(color),
      builder: (ColorSliderController ctrl) {
        return AlertDialog(
          contentPadding: const EdgeInsets.all(0),
          backgroundColor: color,
          insetPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          title: Center(
            child: Text("Adjust Color Channels", style: GoogleFonts.raleway(),),
          ),
          content: Container(
            height: 300.0,
            child: Column(
              children: List.generate(ColorChannels.values.length, (index) {
                return ColorSlider(
                  color: color, 
                  channel: ColorChannels.values[index]
                );
              }),
            ),
          ),
        );
      },
    );
  }
}

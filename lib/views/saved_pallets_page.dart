import 'package:color_back/views/pallet_view_page.dart';
import 'package:color_back/widgets/app_bar.dart';
import 'package:color_back/widgets/app_footer.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';



class SavedPalletsPage extends StatefulWidget {
  const SavedPalletsPage({Key? key}) : super(key: key);

  @override
  State<SavedPalletsPage> createState() => _SavedPalletsPageState();
}

class _SavedPalletsPageState extends State<SavedPalletsPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: MainAppBar(
          title: "Saved Pallets",
        ),
        bottomNavigationBar: const MainAppFooter(),
        body: generateList(context));
  }

  Future<List<Widget>> getAllPrefs(context) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getKeys().map<Widget>((key) {
      return InkWell(
          onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => PalletViewPage(name: key, data: prefs.get(key))),
              );
          } ,
          child: Card(
              elevation: 3.0,
              child: ListTile(
                title: Text(key, style: GoogleFonts.raleway(),),
                trailing: IconButton(
                  icon: Icon(Icons.delete_forever),
                  onPressed: () {
                    showConfirmationDialogue(context, prefs, key);
                  },
                  ),
              )));
    }).toList(growable: false);
  }

  generateList(context) {
    return FutureBuilder<List<Widget>>(
        future: getAllPrefs(context),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return const Center(child:CircularProgressIndicator());
          return ListView(
            children: snapshot.data!,
          );
        });
  }
  
  Future<void> showConfirmationDialogue(BuildContext context, SharedPreferences prefs, String name) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Delete Pallet', style: GoogleFonts.raleway(),),
          content: SingleChildScrollView(
            child: Text('Cofirm removing saved pallet $name?', style: GoogleFonts.raleway()),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Confirm', style: GoogleFonts.raleway(),),
              onPressed: () {
                prefs.remove(name);
                Navigator.of(context).pop();
                //refesh page
                setState(() {});
              },
            ),
            TextButton(
              child: Text('Cancel', style: GoogleFonts.raleway()),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

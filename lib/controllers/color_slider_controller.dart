import 'dart:ui';

import 'package:color_back/enums/enums.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ColorSliderController extends GetxController {
  double  currSliderValue = 0.0;
  Color color;
  ColorSliderController(this.color);

  setColor(newColor) {
    // print("$color(old): $newColor(new)");
    color = newColor;
    update();
  }

  updateSliderValue(newValue){
    currSliderValue = newValue;
    update();
  }

  getSliderValue(channel){
    switch (channel) {
      case ColorChannels.blue:
        currSliderValue = color.blue.roundToDouble();
        break;
      case ColorChannels.red:
        currSliderValue = color.red.roundToDouble();
        break;
      case ColorChannels.green:
        currSliderValue = color.green.roundToDouble();
        break;
      case ColorChannels.alpha:
        currSliderValue = color.alpha.roundToDouble();
        break;
      default:
        currSliderValue = color.opacity.roundToDouble();
        break;
    }
    update();
    return currSliderValue;
  }

  adjustAlpha(double value) {
    setColor(color.withAlpha(value.toInt()));
  }

  adjustRed(double value) {
    setColor(color.withRed(value.toInt()));
  }

  adjustBlue(double value) {
    setColor(color.withBlue(value.toInt()));
  }

  adjustOpacity(double value) {
    setColor(color.withOpacity(value));
  }

  adjustGreen(double value) {
    setColor(color.withGreen(value.toInt()));
  }

  getLabel(enumVal){
    switch (enumVal) {
      case ColorChannels.alpha:
        return "Alpha";
      case ColorChannels.green:
        return "Green";
      case ColorChannels.red:
        return "Red";
      case ColorChannels.blue:
        return "Blue";
      case ColorChannels.opacity:
        return "Opacity";
      default:
        return "Opacity";
    }
  }

  setMaxSlider(val){
    if (val == ColorChannels.opacity) {
      return 1.0;
    }
    else {
      return 255.0;
    }
  }

  setDivisions(val){
    if (val == ColorChannels.opacity) {
      return 10;
    }
    else {
      return 255;
    }
  } 

  getFunc(value) {
    switch (value) {
      case ColorChannels.alpha:
        return adjustAlpha;
      case ColorChannels.blue:
        return adjustBlue;
      case ColorChannels.green:
        return adjustGreen;
      case ColorChannels.red:
        return adjustRed;
      case ColorChannels.opacity:
        return adjustOpacity;
      default:
        return adjustOpacity;
    }
  }
}
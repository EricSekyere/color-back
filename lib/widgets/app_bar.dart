import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../controllers/home_page_controller.dart';

class MainAppBar extends StatelessWidget with PreferredSizeWidget {
  final String title;
  final bool showSaveIcon;
  MainAppBar({Key? key, required this.title, this.showSaveIcon = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomePageController>(
        init: HomePageController(),
        builder: (ctrl) {
          return AppBar(
              title: Center(
                  child: Text(title,
                      style: GoogleFonts.raleway(fontWeight: FontWeight.bold))),
              actions: appBarActions(context, ctrl));
        });
  }

  List<Widget> appBarActions(BuildContext context, HomePageController ctrl) {
    List<Widget> actions = [];
    if (showSaveIcon) {
      actions.add(IconButton(
        onPressed: () {
          if (ctrl.colors.length > 1) {
            ctrl.savePalletDialogue(context);
          }
          
        },
        icon: const Icon(Icons.save_alt_outlined),
        tooltip: "Save Pallet",
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ));
    }
    return actions;
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

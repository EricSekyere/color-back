import 'package:color_back/views/cmix_page.dart';
import 'package:color_back/views/saved_pallets_page.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MainAppDrawer extends StatelessWidget {
  const MainAppDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      elevation: 2.6,
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
              decoration: const BoxDecoration(
                color: Colors.blue,
              ),
              child: Center(
                child: Text(
                  'Color Background',
                  style: GoogleFonts.raleway(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    decorationThickness: 5,
                  ),
                ),
              )),
          ListTile(
            leading: const Icon(Icons.save_alt_outlined),
            title: Text('Saved Pallets', style: GoogleFonts.raleway(),),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const SavedPalletsPage()),
              );
            },
          ),
          ListTile(
            leading: const Icon(Icons.construction_rounded),
            title: Text('Cmix (coming soon)', style: GoogleFonts.raleway()),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const CmixPage()),
              );
            },
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomText extends StatelessWidget {
  final String text;

  const CustomText({Key? key, required this.text, int? size})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Text txt = Text(text,
        style: GoogleFonts.raleway(fontWeight: FontWeight.bold));
    return txt;
  }
}

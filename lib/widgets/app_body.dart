import 'package:color_back/widgets/color_setting.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../controllers/home_page_controller.dart';

class MainAppBody extends StatelessWidget {
  final HomePageController controller = Get.put(HomePageController());
  
  MainAppBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                onPressed: () => { controller.changeColor() },
                // ignore: prefer_const_constructors
                child: Text("Change Color",
                    style: GoogleFonts.raleway(fontWeight: FontWeight.bold)),
              ),
              ElevatedButton(
                onPressed: () => {
                  if(controller.colors.isNotEmpty) {
                    controller.clearColors(context)
                  }
                },
                // ignore: prefer_const_constructors
                child: Text("Clear Colors",
                    style: GoogleFonts.raleway(fontWeight: FontWeight.bold)),
              )
            ],
          ),
          GetBuilder<HomePageController>(
            builder: (_idx) =>
              Expanded(
                child: ListView.builder(
                  shrinkWrap: true,
                  padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                  itemCount: _idx.colors.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Card(
                      elevation: 6.0,
                      margin: const EdgeInsets.all(10),
                      shape: Border(
                        bottom: BorderSide(color: controller.colors.elementAt(index), width: 4),
                        left: BorderSide(color: controller.colors.elementAt(index), width: 4),
                        right: BorderSide(color: controller.colors.elementAt(index), width: 4),
                        top: BorderSide(color: controller.colors.elementAt(index), width: 4)
                      ),
                      child: ListTile(
                        title: Center(child: Text("${index + 1}. ${controller.colors.elementAt(index)}",
                          style: GoogleFonts.raleway(fontWeight: FontWeight.bold)
                        )),
                        textColor: controller.colors.elementAt(index),
                        onTap: () { controller.changeColor(controller.colors.elementAt(index));},
                        visualDensity: VisualDensity.adaptivePlatformDensity,
                        trailing: IconButton(
                          icon: Icon(Icons.settings),
                          onPressed: (){
                            controller.changeColor(controller.colors.elementAt(index));
                            showDialog(
                              context: context, 
                              builder: (builder) {
                                var x = ColorSettings(color: _idx.bgColor);
                                return x;
                              }
                            );
                          },
                        )
                      ),
                    );
                  }
                )
              ),
            ), 
          ]
        ),
      );
  }
}
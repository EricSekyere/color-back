import 'dart:math';

import 'package:color_back/controllers/color_slider_controller.dart';
import 'package:color_back/models/models.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class HomePageController extends GetxController {
  Color bgColor = Colors.amber;
  Set<Color> colors = {};
  Set<Color> backupColors = {};
  final TextEditingController textController = TextEditingController();
  late ColorSliderController colorSliderController;

  void changeColor([Color? color]) {
    if (color != null) {
      bgColor = color;
    } else {
      dynamic rndColor = Color(getRandomInt()).withOpacity(1.0);
      bgColor = rndColor;
      if (colors.length == 30) {
        colors.remove(colors.first);
      }
      colors.add(bgColor);
    }
    update();
  }

  void clearColors(BuildContext context) {
    // backup the current set of colors
    backupColors = Set.from(colors);
    colors.clear();
    bgColor = Colors.amber;
    showToast(context, text: "Colors successfully cleared.");
    update();
  }

  void restorePreviousColors() {
    colors = Set.from(backupColors);
    if (colors.isNotEmpty) {
      bgColor = colors.elementAt(0);
    }
    // clear out the backup
    backupColors.clear();
    update();
  }

  int getRandomInt() {
    return (Random().nextDouble() * 0xFFFFFF).toInt();
  }

  Future savePallet() async {
    PalletData data = PalletData(textController.text, colors.map((e) => e.toString()).toList());
    var saver = PalletSaver();
    await saver.save(data);
  }

  void savePalletDialogue(context) {
    if(colors.isNotEmpty) {
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Center(
                child:Text("Save Pallet", style: GoogleFonts.raleway())
              ),
              content: Container(
                height: 80.0,
                child: TextField(
                  controller: textController,
                  decoration: InputDecoration(
                    hintText: 'Pallet Name',
                    hintStyle: GoogleFonts.raleway(),
                    prefixIcon: const Icon(Icons.edit),
                    border: const OutlineInputBorder(
                      borderSide: BorderSide(width: 5.0)
                      )
                  ),
                ),
              ),
              actions: <Widget>[
                ElevatedButton(
                    onPressed: ()  async {
                      if (colors.isNotEmpty){
                        await savePallet();
                        textController.clear();
                        Navigator.pop(context);
                      }
                      else {
                        null;
                      }
                    },
                    child: Text("Save pallet", style: GoogleFonts.raleway()))
              ],
            );
          });
    }

  }

  void showToast(BuildContext context, {String text = ''}) {
    final scaffold = ScaffoldMessenger.of(context);
    scaffold.showSnackBar(SnackBar(
      content: Text(text, style: GoogleFonts.raleway()),
      action: SnackBarAction(
        label: "UNDO",
        onPressed: ()  {
          restorePreviousColors();
        },
      ),
      duration: const Duration(seconds: 3),
    ));
  }

  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }
}

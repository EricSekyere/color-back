import 'package:shared_preferences/shared_preferences.dart';

import '../exceptions/exceptions.dart';

class PalletData {
  String key;
  final List<String> pallet;

  PalletData(this.key,  this.pallet);
}


class PalletSaver {
  Future save(PalletData data) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.get(data.key) == null) {
        prefs.setStringList(data.key, data.pallet);
    }
    else {
      throw PalletSaverException("Pallet name already exists");
    }
  }

  getAllKeys() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getKeys();
  }

  Future remove(PalletData data) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(data.key);
  }
  
}
import 'package:color_back/controllers/home_page_controller.dart';
import 'package:color_back/widgets/app_body.dart';
import 'package:color_back/widgets/app_drawer.dart';
import 'package:color_back/widgets/app_footer.dart';
import 'package:color_back/widgets/app_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomePage extends StatelessWidget {

  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomePageController>(
      init: HomePageController(),
      builder: (ctrl) {
      return Scaffold(
          backgroundColor: ctrl.bgColor,
          appBar: MainAppBar(title: "Color Background", showSaveIcon: true,),
          drawer: const MainAppDrawer(),
          body: MainAppBody(),
          bottomNavigationBar: const MainAppFooter());
    });
  }
}

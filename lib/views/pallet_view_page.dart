import 'package:color_back/enums/enums.dart';
import 'package:color_back/widgets/app_bar.dart';
import 'package:color_back/widgets/app_footer.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class PalletViewPage extends StatefulWidget {
  final String name;
  final dynamic data;
  const PalletViewPage({Key? key, required this.name, required this.data})
      : super(key: key);

  @override
  State<PalletViewPage> createState() =>
      _PalletViewPageState(key: this.key, name: this.name, data: this.data);
}

class _PalletViewPageState extends State<PalletViewPage> {
  final String name;
  final dynamic data;
  var gradient = ColorGradient.linear;
  bool isSwitched = false;
  bool hideOverlay = false;

  _PalletViewPageState({Key? key, required this.name, required this.data});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: MainAppBar(title: name),
        bottomNavigationBar: const MainAppFooter(),
        body: Container(
            height: 900,
            width: double.infinity,
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 30),
            decoration: BoxDecoration(gradient: setGradient()),
            child: IntrinsicHeight(
                child: Column(children: generatePallet(data)))));
  }

  List<Color> dataToColors(data) {
    List<Color> colors = [];
    for (String color in data) {
      String hexValue = color.replaceAll("Color(", "").replaceAll(")", "");
      colors.add(Color(int.parse(hexValue)));
    }
    return colors;
  }

  List<Widget> generatePallet(data) {
    var colors = dataToColors(data);
    List<Widget> children = [];
    if (!hideOverlay) {
      for (Color color in colors) {
        children.add(Expanded(
          child: Container(
              width: double.infinity,
              decoration: BoxDecoration(
                color: color.withOpacity(1),
              ),
              child: Center(
                  child: Text(
                "0x${color.value.toRadixString(16)}",
                style: GoogleFonts.raleway(),
              ))),
        ));
      }
    }

    children.add(const Spacer());
    children.add(controlsWidgets());

    return children;
  }

  Gradient setGradient() {
    switch (gradient) {
      case ColorGradient.radial:
        return RadialGradient(
          colors: dataToColors(data),
          radius: 1.0
        );
      case ColorGradient.sweep:
        return SweepGradient(colors: dataToColors(data));
      default:
        return LinearGradient(
          colors: dataToColors(data),
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
        );
    }
  }

  Widget toggleButtons() {
    List<bool> _selections = List.generate(3, (_) {
      return false;
    });
    return Column(children: [
      ToggleButtons(
        children: const <Widget>[
          Icon(
            Icons.linear_scale,
            semanticLabel: "Linear",
          ),
          Icon(
            Icons.radar,
            semanticLabel: "Radial",
          ),
          Icon(
            Icons.wifi_protected_setup,
            semanticLabel: "Sweep",
          )
        ],
        isSelected: _selections,
        renderBorder: true,
        fillColor: Colors.amber,
        onPressed: (int index) {
          setState(() {
            _selections[index] = !_selections[index];
            switch (index) {
              case 1:
                gradient = ColorGradient.radial;
                break;
              case 2:
                gradient = ColorGradient.sweep;
                break;
              default:
                gradient = ColorGradient.linear;
            }
          });
        },
      ),
      Text(
        "Toggle Background Gradient",
        style: GoogleFonts.raleway(),
      )
    ]);
  }

  Widget switchButton() {
    return Column(children: [
      Row(children: [
        Text(
          "OFF",
          style: GoogleFonts.raleway(),
        ),
        Switch(
          onChanged: toggleSwitch,
          value: isSwitched,
          activeColor: Colors.green,
          activeTrackColor: Colors.yellow,
          inactiveThumbColor: Colors.redAccent,
          inactiveTrackColor: Colors.orange,
        ),
        Text(
          "ON",
          style: GoogleFonts.raleway(),
        )
      ]),
      Text(
        "Hide Overlay",
        style: GoogleFonts.raleway(),
      )
    ]);
  }

  void toggleSwitch(bool value) {
    setState(() {
      isSwitched = !isSwitched;
      if (isSwitched) {
        hideOverlay = true;
      } else {
        hideOverlay = false;
      }
    });
  }

  Widget controlsWidgets() {
    return Row(
      children: [toggleButtons(), Spacer(), switchButton()],
    );
  }
}

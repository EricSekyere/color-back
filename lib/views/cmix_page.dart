import 'package:color_back/widgets/app_bar.dart';
import 'package:color_back/widgets/app_footer.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CmixPage extends StatelessWidget {
  const CmixPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: MainAppBar(title: "CMIX"),
        bottomNavigationBar: const MainAppFooter(),
        body: Center(
          child: Text(
            "Coming Soon",
            style: GoogleFonts.raleway(),
          ),
        ));
  }
}

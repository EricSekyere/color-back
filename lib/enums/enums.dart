enum ColorChannels {
  blue,
  red,
  alpha,
  green,
  opacity
}

enum ColorGradient{
  linear,
  radial,
  sweep
}